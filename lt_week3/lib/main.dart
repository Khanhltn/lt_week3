import 'dart:convert';

String jsonRaw = '{ "id": 1, "url": "https://via.placeholder.com/600/92c952&quot"}';

class ImageModel {
  late int id;
  late String url;

  ImageModel(this.id, this.url);

  ImageModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    url = json['url'];
  }

  Map<String, dynamic> toJson() =>
      {
        'id': id,
        'url': url,
      };

  String toString() {
    return '($id, $url)';
  }
}

void main() {
  Map<String, dynamic> imageModelMap = jsonDecode(jsonRaw);
  var image1 = ImageModel.fromJson(imageModelMap);
  print(image1);
}